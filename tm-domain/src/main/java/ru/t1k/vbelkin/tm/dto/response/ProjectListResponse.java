package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @NotNull
    private List<Project> projects;

    public ProjectListResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
