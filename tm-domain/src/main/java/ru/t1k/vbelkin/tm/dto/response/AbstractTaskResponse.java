package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class AbstractTaskResponse extends AbstractResponse {

    private Task task;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

}
