package ru.t1k.vbelkin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
