package ru.t1k.vbelkin.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
