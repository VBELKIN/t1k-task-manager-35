package ru.t1k.vbelkin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.comparator.CreatedComparator;
import ru.t1k.vbelkin.tm.comparator.NameComparator;
import ru.t1k.vbelkin.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator comparator;

    Sort(@Nullable String displayName, @Nullable Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
