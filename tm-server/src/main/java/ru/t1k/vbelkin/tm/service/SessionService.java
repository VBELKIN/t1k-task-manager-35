package ru.t1k.vbelkin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.repository.ISessionRepository;
import ru.t1k.vbelkin.tm.api.service.ISessionService;
import ru.t1k.vbelkin.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
