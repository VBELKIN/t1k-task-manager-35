package ru.t1k.vbelkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setTaskId(id);
        request.setStatus(Status.IN_PROGRESS);
        getTaskEndpointClient().changeTaskStatusById(request);
    }

}
