package ru.t1k.vbelkin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        // @NotNull DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(getToken());
        //getDomainEndpointClient().loadDataJsonFasterXml(request);
    }

}
