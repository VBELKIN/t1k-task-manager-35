package ru.t1k.vbelkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

}
