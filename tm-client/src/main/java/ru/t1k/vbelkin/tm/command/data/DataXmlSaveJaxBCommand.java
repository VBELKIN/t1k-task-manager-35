package ru.t1k.vbelkin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        // @NotNull DataXmlSaveJaxbRequest request = new DataXmlSaveJaxbRequest(getToken());
        //getDomainEndpointClient().saveDataXmlJaxB(request);
    }

}
